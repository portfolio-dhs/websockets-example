'use strict';

const http = require('http');
const express = require('express');
const chalk = require('chalk');

const app = express();
const SocketController = require('./controllers/SocketController');

const server = http.createServer(app);
const PORT = process.env.NODE_PORT || 3000;

app.use(express.static('./public'));

// noinspection JSUnusedLocalSymbols
// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  console.log(chalk.yellow(`[INFO] ${err.message}`));
  if (err.message.match(/not found/)) {
    return res.status(404).send({ error: err.message });
  }
  console.log(`Error: ${err.message}`);
  return res.status(500).send({ error: err.message });
});

const handleFatalError = (err) => {
  console.log(chalk.red(`[ERROR]: ${err.message}\n\n${err.stack}`));
  process.exit(1);
};

if (!module.parent) {
  process.on('uncaughtException', handleFatalError);
  process.on('unhandledRejection', handleFatalError);

  const sc = new SocketController(server);
  sc.handleSockets();

  server.listen(PORT, () => {
    console.log(
      `${chalk.green('[OK]')} Server is ${chalk.green(
        'LISTENING'
      )} on port ${PORT}`
    );
  });
}

module.exports = server;
