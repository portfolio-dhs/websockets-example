const chalk = require('chalk');
const axios = require('axios');

class SocketController {
  constructor(server) {
    this.server = server;
    this.count = 0;
  }

  handleSockets() {
    const io = require('socket.io')(this.server);

    io.on('connection', (socket) => {
      console.log(
        chalk.cyan(
          '[INFO] Someone has connected through sockets. ID: ' + socket.id
        )
      );

      socket.on('new-number', (data) => {
        console.log(
          chalk.cyan('[INFO] New number arrived: ' + JSON.stringify(data))
        );
        this.count++;
        io.sockets.emit('counter-change', this.count);

        axios
          .post('https://api.envia.com/ship/generaltrack/', {
            trackingNumbers: data,
          })
          .then((response) => {
            socket.emit('message', response.data);
          })
          .catch((error) => console.log(error));
      });
    });
  }
}

module.exports = SocketController;
