# Instructions

### Step 1: Install dependencies:

Run the command:

```
npm install
```

### Start in Production mode:

```
npm run start
```

to start the server. If the port is not specified in a .env
with the env var NODE_PORT then the port 3000 will be use by default.

A simple frontend has been developed with the purpose of testing the backend.
Access to it via: `http://localhost:3000`.

You fill the input with the tracking codes dividing them with a `,` and then
press enter or pres the submit button

### Start in Development mode:

```
npm run start-dev
```

After being run by this way the server will be using nodemon. This will
allow to update the server automatically when a change on the source code
is detected.

### Lint code

Run:

```
npm run lint
```

for linting the code. If you want to apply fixes to the code,
then run:

```
npm run lint-fix
```

### Format code

Run:

```
npm run format
```

to format code using prettier.
